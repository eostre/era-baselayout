#!/usr/bin/env sh

mkdir build ||:
cp -r tasks build
cd build

cat << __EOF >> POSTEXTRACT
#!/usr/bin/env sh
for TASK_file in tasks/*; do
sh \$TASK_file; done
rm -r exec POSTEXTRACT
__EOF
chmod +x POSTEXTRACT

../../tarsh/mktarsh . stage2
