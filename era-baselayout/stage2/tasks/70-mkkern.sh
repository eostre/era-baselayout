#!/usr/bin/env sh

emerge --oneshot --ask --verbose --noreplace sys-kernel/gentoo-sources

cp config/kconfig /usr/src/linux/.config
cd  /usr/src/linux
nice make -j$(nproc)

make install
make modules_install
