#!/usr/bin/env sh

install -d /etc/runlevels/graphics
install -d /etc/runlevels/networking
install -d /etc/runlevels/powersave
install -d /etc/runlevels/printing
install -d /etc/runlevels/xen

rc-update add xdm graphics

for NET_SERVICE in "NetworkManager avahi-daemon avahi-dnsconfd distccd dnsmasq netmount nfsclient ntp-client sshd"; do
	rc-update add $NET_SERVICE networking; done
	rc-update add -s networking default

for POWERSAVE_SERVICE in "acpid laptop_mode"; do
	rc-update add $POWERSAVE_SERVICE powersave; done
	rc-update add -s powersave default

for PRINTING_SERVICE in "cups-browesd cupsd"; do
	rc-update add $PRINTING_SERVICE printing; done
	rc-update add -s printing default

for XEN_SERVICE in "xen-watchdog xencommons xenconsoled xendomains xenstored"; do
	rc-update add $XEN_SERVICE xen; done
	rc-update add -s xen default

for BOOT_SERVICE in "apparmor dmcrypt"; do
	rc-update add $BOOT_SERVICE boot; done
