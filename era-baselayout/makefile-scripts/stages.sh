#!/usr/bin/env sh

DESTDIR=${DESTDIR:-tmp}

cd stage1
make build image
cd ..
cd stage2
make buildexec
