#!/usr/bin/env sh
cd releases
cp -r ../era-baselayout/ era-baselayout-$(cat version)
tar czvf era-baselayout-$(cat version).tar.gz era-baselayout-$(cat version)/
scp era-baselayout-*.tar.gz root@pkg.e9d.org:/srv/http/distfiles
